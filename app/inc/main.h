#ifndef _MAIN_H_
#define _MAIN_H_


#ifdef __cplusplus
extern "C" {
#endif


//------------------------Funciones-------------------------
int main(void);
void ActualizarDAC(void);
void ActualizarBufferSalidaDAC(void);
//----------------------------------------------------------

//--------------------Variables-globales--------------------
uint16_t BufferSalidaDAC[512];
uint8_t dmaChannelNum;
uint32_t DMAbuffer[100];
uint32_t UltimoValorPotencimentro;
//----------------------------------------------------------


#ifdef __cplusplus
}
#endif


#endif

