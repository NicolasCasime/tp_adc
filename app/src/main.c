//------------------Includes-----------------------
#include "board.h"
#include "main.h"
#include <cr_section_macros.h>
#include <math.h>
//-------------------------------------------------

void SysTick_Handler(void)
{
	//aca se entra cada 0,01ms
	static int cont=0;
	cont++;
	if(cont==2)
	{
		ActualizarDAC();
		cont=0;
	}
}


void ActualizarDAC(void)
{
	//A esta funcion se entra cada 0,02 ms.
	static uint16_t Muestra=0;

	Chip_DAC_UpdateValue(LPC_DAC, BufferSalidaDAC[Muestra]);
	Muestra++;
	if(Muestra==512)
	{
		Muestra=0;
		ActualizarBufferSalidaDAC();
	}

}

int main (void)
{


	SystemCoreClockUpdate();
	Board_Init();
	SysTick_Config(SystemCoreClock/100000);//De esta forma se llama al systick cada 0,01ms

	//-----------------------------------Configuro-DAC---------------------------------(Afanado de ejemplo)
	/* Configuración del pin P0.26 como AOUT */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 26, IOCON_FUNC2);
	/* Inicialización del DAC */
	Chip_DAC_Init(LPC_DAC);
	//---------------------------------------------------------------------------------

	//------------------------------------Configuro-ADC--------------------------------(Afanado de ejemplo)
	ADC_CLOCK_SETUP_T ADCSetup;
	/* Configuración del pin P0.23 como AD0.5 (J6-20 en el stick) */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 23, IOCON_FUNC1);
	/* Configuración del ADC */
	Chip_ADC_Init(LPC_ADC, &ADCSetup);
	//Chip_ADC_SetSampleRate(LPC_ADC, &ADCSetup, 20000);
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_Int_SetGlobalCmd(LPC_ADC, ENABLE);
	NVIC_DisableIRQ(ADC_IRQn);
	Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);
	/* Inicialización del controlador GPDMA */
	Chip_GPDMA_Init(LPC_GPDMA);
	NVIC_EnableIRQ(DMA_IRQn);

	dmaChannelNum = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_ADC);

	Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum, GPDMA_CONN_ADC, (uint32_t) &DMAbuffer, GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, 100);
	//---------------------------------------------------------------------------------
	while(1);


	return 0;
}



void DMA_IRQHandler (void)//Termino de leer 100 valores del potenciomentro, los promedio, los guardo en una variable global, y vuelvo a disparar.
{
	int i;
	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum) == SUCCESS)
	{
		uint32_t SumaTotal=0;
		for(i=0;i<100;i++)
		{
			SumaTotal+=ADC_DR_RESULT(DMAbuffer[i]);
		}
		UltimoValorPotencimentro=SumaTotal/100;

		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum, GPDMA_CONN_ADC, (uint32_t) &DMAbuffer, GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, 100);
	}
}



void ActualizarBufferSalidaDAC(void)
{
	//Esta funcion debe cargar los proximos 512 valores del DAC. Dependiendo de la frecuencia deseada, un ciclo entero tendra mas o menos muestras
	float FrecuenciaDeseada;
	FrecuenciaDeseada=(UltimoValorPotencimentro*10000)/4096;//Por regla de 3 simples, esta es la frecuencia que deseo crear
	float PeriodoDeseado=1/FrecuenciaDeseada;//Inversa de la frecuencia, este es el periodo que deseo obtener
	float Resolucion=50000;//Esto es cada cuanto el DAC actualiza un valor (lo hace cada 0,02 mili)
	int CantidadMuestras=PeriodoDeseado*Resolucion;//Esta variable tiene la cantidad de muestras que se deben tomar en un ciclo.

	bool BufferCompleto=false;
	uint16_t Posicion=0;
	uint16_t aux;
	float Parcial;


	while(1)
	{
		for(aux=0;aux<CantidadMuestras;aux++)
		{
			Parcial=(float)(2*3.1415) /CantidadMuestras;
			Parcial=Parcial*aux;
			Parcial=sin(Parcial);
			Parcial=Parcial*512;
			Parcial=Parcial+512;
			BufferSalidaDAC[Posicion]=Parcial;
			//BufferSalidaDAC[Posicion]=(uint16_t) (   sin( ((float)(2*3.1415) /CantidadMuestras)*aux)  )*1024;
			Posicion++;
			if(Posicion==512)
			{
				BufferCompleto=true;
				break;
			}
		}
		if(BufferCompleto)
			break;
	}

}
